const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const helmet = require('helmet');
const routes = require('./routes');

const port = process.env.PORT || 3333;
const app = express();

//middlewares
app.use(cors());
app.use(morgan('tiny'));
app.use(helmet());
app.use(express.json());
app.use(routes);

app.listen(port, () => console.log(`Listening at http://localhost:${port}`));
