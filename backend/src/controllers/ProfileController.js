const connection = require('../database/connection');

//busca por incidents de uma ong especifica
module.exports = {
  async index(req, res) {
    const ong_id = req.headers.authorization;

    const incidents = await connection('incidents')
      .where('ong_id', ong_id)
      .select('*');

    return res.json(incidents);
  },
};
